
# LYNCE DEV 
## About The Project
This project intends to provide a virtual ateliè to Esdras Costa, this project's client,  who's a talented artist that expects to expand his business through an online portfolio. The development method used is a hybrid one, it has been chosen to use element both from the Scrum and XP devepment methods. 

## About the Team
| Members | E-mail | GitLab |
| :-----: | :----: | :----: |
| Álax de Carvalho Alves | alaxallves@gmail.com | [alaxalves](https://gitlab.com/alaxalves) |
| Matheus Batista Silva | matheusbattista@hotmail.com |  [matheusbsilva](https://gitlab.com/matheusbsilva) |
| Ysaac Kalled Souza Pontes | ysaackalled@gmail.com | [YsaacKalled] (https://gitlab.com/YsaacKalled)|
| Andre de Sousa Costa Filho | andre.filho001@outlook.com | [andre.filho](https://gitlab.com/andre.filho) |
| Iago Vasconcelos de Carvalho| IagoVC_2012@hotmail.com | [IagoCarvalho](https://gitlab.com/Iago_Carvalho) |

# DEPENDENCIES:

- Python 2.7
- Django 10.2
- SQLite 
